<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<!--[if lt IE 9]>
  <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
<![endif]-->
  <meta name="viewport" content="width=device-width">
  <meta http-equiv="content-language" content="ru" > <?php // TODO - в будущем генетить автоматом ?>
  <meta name='yandex-verification' content='4b9b010720b20f08' />
<?php
  //Регистрируем файлы скриптов в <head>
  if (YII_DEBUG) {
    Yii::app()->assetManager->publish(YII_PATH.'/web/js/source', false, -1, true);
  }

  Yii::app()->clientScript->registerCoreScript('jquery.project');
  Yii::app()->clientScript->registerCoreScript('bootstrap');
  $bootstrapFont = Yii::getPathOfAlias('application.assets.bootstrap.fonts') . DIRECTORY_SEPARATOR;
  Yii::app()->clientScript->addDependResource('bootstrap.min.css', array(
    $bootstrapFont.'glyphicons-halflings-regular.eot' =>  '../fonts/',
    $bootstrapFont.'glyphicons-halflings-regular.svg' =>  '../fonts/',
    $bootstrapFont.'glyphicons-halflings-regular.ttf' =>  '../fonts/',
    $bootstrapFont.'glyphicons-halflings-regular.woff' => '../fonts/',
  ));

  Yii::app()->clientScript->registerScriptFile('/themes/business/js/js.js', CClientScript::POS_HEAD);
  
  Yii::app()->clientScript->registerScript('setScroll', "setAnchor();", CClientScript::POS_READY);
  Yii::app()->clientScript->registerScript('menu.init', "$('.dropdown-toggle').dropdown();", CClientScript::POS_READY);

  Yii::app()->clientScript->registerCssFile('/themes/business/css/content.css');
  Yii::app()->clientScript->registerCssFile('/themes/business/css/page.css');
?>
<link href='http://fonts.googleapis.com/css?family=Roboto:400,400italic,300italic,300,100italic,100,500italic,500,700italic,700,900,900italic&subset=latin,cyrillic-ext,cyrillic' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
  <title><?php echo CHtml::encode($this->getPageTitle()); ?></title>
</head>
<body>
  <div id="wrap" class="container">
    <div id="head" class="row">
		<div class="container">
<?php if (Yii::app()->request->url == "/"){ ?>
      <div class="logo"><img border="0" src="/themes/business/gfx/logo.png"></div>
<?php } else { ?>
      <a href="/" title="Главная страница" class="logo"><img src="/themes/business/gfx/logo.png"></a>
<?php }?>
      <div class="cname">Объединённая профсоюзная организация<br>
	  Жешартского фанерного комбината
      </div>
      <div class="tright">
        <div class="numbers">
         <i class="fa fa-phone"></i> +7 (82134) 47-1-20<br>
		 <p>Республика Коми, п. Жешарт, ул. Гагарина, д. 1.</p>
		 <p>E-mail: <a href="mailto:btv@zfk.ru">btv@zfk.ru</a></p>
        </div>
      </div>
	  </div>
    </div>
    <div class="b-menu-top navbar navbar-default" role="navigation">
      <div class="container">
<?php

if (Yii::app()->hasModule('search')) {
  $this->widget('SearchWidget');
}
$this->widget('MenuWidget', array(
  'rootItem' => Yii::app()->menu->all,
  'htmlOptions' => array('class' => 'nav navbar-nav'), // корневой ul
  'submenuHtmlOptions' => array('class' => 'dropdown-menu'), // все ul кроме корневого
  'activeCssClass' => 'active', // активный li
  'activateParents' => 'true', // добавлять активность не только для конечного раздела, но и для всех родителей
  //'labelTemplate' => '{label}', // шаблон для подписи
  'labelDropDownTemplate' => '{label} <b class="caret"></b>', // шаблон для подписи разделов, которых есть потомки
  //'linkOptions' => array(), // атрибуты для ссылок
  'linkDropDownOptions' => array('data-target' => '#', 'class' => 'dropdown-toggle', 'data-toggle' => 'dropdown'), // атрибуты для ссылок для разделов, у которых есть потомки
  'linkDropDownOptionsSecondLevel' => array('data-target' => '#', 'data-toggle' => 'dropdown'), // атрибуты для ссылок для разделов, у которых есть потомки
  //'itemOptions' => array(), // атрибуты для li
  'itemDropDownOptions' => array('class' => 'dropdown'),  // атрибуты для li разделов, у которых есть потомки
  'itemDropDownOptionsSecondLevel' => array('class' => 'dropdown-submenu'),
//  'itemDropDownOptionsThirdLevel' => array('class' => ''),
  'maxChildLevel' => 2,
  'encodeLabel' => false,
));

?>
      </div>
    </div>

<?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_TOP)); ?>

<?php // + Главный блок ?>
    <div id="main">

      <div id="container" class="container">
<?php

$column1 = 0;
$column2 = 9;
$column3 = 0;

if (Yii::app()->menu->current != null) {
  $column1 = 3;
  $column2 = 6;
  $column3 = 3;
  
  if (Yii::app()->menu->current->getCountModule(SiteModule::PLACE_LEFT) == 0) {$column1 = 0; $column3 = 4;}
  if (Yii::app()->menu->current->getCountModule(SiteModule::PLACE_RIGHT) == 0) {$column3 = 0; $column1 = $column1*4/3;}
  $column2 = 12 - $column1 - $column3;
  //if ($column2 == 12) $column2 = 9;
}

?>
        <?php if ($column1 > 0): // левая колонка ?>
		<?php if (Yii::app()->request->url !== "/") { ?>
        <div id="sidebarLeft" class="col-md-<?php echo $column1; ?>">
		<?php } else {?>
		 <div id="sidebarLeft" class="col-md-<?php echo $column2; ?>">
		<?php }?>
          <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_LEFT)); ?>
        </div>
        <?php endif ?>
        

		 <?php if (Yii::app()->request->url !== "/") { ?>
        <div id="content" class="col-md-<?php echo $column2; ?>">

       
          <div class="page-header">
            <h1> <?php echo $this->caption; ?> </h1>
          </div>
         

          
          <?php if ($this->useBreadcrumbs && isset($this->breadcrumbs)): // Цепочка навигации ?>
          <?php $this->widget('BreadcrumbsWidget', array(
            'homeLink' => array('Главная' => Yii::app()->homeUrl),
            'links' => $this->breadcrumbs,
          )); ?>
          <?php endif ?>

          <div class="cContent">
            <?php echo $content; ?>
          </div>
          <?php //$this->widget('BlockWidget', array("place" => SiteModule::PLACE_BOTTOM)); ?>
        </div>
		<?php } ?>

        <?php if ($column3 > 0): // левая колонка ?>
				<?php if (Yii::app()->request->url !== "/") { ?>
        <div id="sidebarRight" class="col-md-<?php echo $column1; ?>">
		<?php } else {?>
		 <div id="sidebarRight" class="col-md-<?php echo $column2; ?>">
		<?php }?>
          <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_RIGHT)); ?>
        </div>
        <?php endif ?>

      </div>
<?php //Тут возможно какие-нить модули снизу ?>
 <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_BOTTOM)); ?>
      <div class="clr"></div>
    </div>
<?php // - Главный блок ?>

<div id="back-top"><span>↑</span></div>
    
  </div>


  <div id="footer">
    <div class="container">
      <div class="logo">
        <img src="/themes/business/gfx/logo_footer.png">
      </div>
	  <div class="cname">Объединённая профсоюзная организация<br>
	  Жешартского фанерного комбината
      </div>
      <div class="">
        <?php $this->widget('BlockWidget', array("place" => SiteModule::PLACE_FOOTER)); ?>
      </div>
	        <div class="tright">
        <div class="numbers">
         <i class="fa fa-phone"></i> +7 (82134) 47-1-20<br>
		 <p>Республика Коми, п. Жешарт, ул. Гагарина, д. 1.</p>
		 <p>E-mail: <a href="mailto:btv@zfk.ru">btv@zfk.ru</a></p>
        </div>
      </div>
    </div>
    <!-- Yandex.Metrika informer -->
<a href="https://metrika.yandex.ru/stat/?id=30012474&amp;from=informer"
target="_blank" rel="nofollow"><img src="//bs.yandex.ru/informer/30012474/3_1_FFFFFFFF_EFEFEFFF_0_pageviews"
style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)" onclick="try{Ya.Metrika.informer({i:this,id:30012474,lang:'ru'});return false}catch(e){}" /></a>
<!-- /Yandex.Metrika informer -->

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter30012474 = new Ya.Metrika({
                    id:30012474,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/30012474" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->
  </div>

</body>
</html>