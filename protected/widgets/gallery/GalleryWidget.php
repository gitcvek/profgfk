<?php

class GalleryWidget extends DaWidget  implements IParametersConfig {

	public $maxAlbums = null;

  public static function getParametersConfig() {
    return array(
      'maxAlbums' => array(
        'type' => DataType::INT,
        'default' => 5,
        'label' => 'Количество отображаемых альбомов',
        'required' => true,
      ),
    );
  }

  public function getGallery() {
    return PhotogalleryPhoto::model()->last($this->maxAlbums)->findAll(['group'=>'id_photogallery_instance']);
  }
  
  public function init() {
    if ($this->maxAlbums === null) {
      $this->maxAlbums = 5;
    }
    parent::init();
  }
  
  public function run() {
    $this->render('galleryWidget');
  }
}
