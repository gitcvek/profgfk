<div class="gallery-item">
  <?php if (($preview = $model->getImagePreview('_onmain')) !== null): ?>
    <div class="gallery-photo">
      <?php echo CHtml::link(CHtml::image($preview->getUrlPath(), $model->name), $model-> gallery ->getUrl(), array('title' => $model->name)); ?>
    </div>
  <?php endif; ?>
</div>



