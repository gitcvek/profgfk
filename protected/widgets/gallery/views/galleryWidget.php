<?php
$this->registerCssFile('gallery.css');
?>
<div class="b-gallery-widget">
<div class="container">
<div class="gallery-container">
  <h2>Фотогалерея</h2>
  <?php
  //если нужно брать переопределенное представление элемента списка из темы,
  //то вместо news.views._list_item нужно прописать
  //webroot.themes.название_темы.views.news._list_item
  ?>
<?php foreach ($this->getGallery() as $model): ?>
<?php $this->render('_listitem', array('model' => $model)); ?>
<?php endforeach; ?>
</div>
<div class="archive"><a href="<?php echo Yii::app()->createUrl('/photogallery/photogallery/index/');?>">Все фотоальбомы</a></div>
</div>
</div>