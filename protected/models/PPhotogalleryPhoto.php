<?php 
  class PPhotogalleryPhoto extends PhotogalleryPhoto {

  	public function behaviors() {
      return array(
               'ImagePreviewBehavior' => array(
                 'class' => 'ImagePreviewBehavior',
                 'imageProperty' => 'image',
                 'formats' => array(
                   '_list' => array(
                     'width' => 288,
                     'height' => 230,
                   ),
                   '_onmain' => array(
                   	'width' => 200,
                   	'height' => 133,
                   ),
                 ),
               ),
      );
    }

    public function relations() {
      return CMap::mergeArray(parent::relations(), array(
        'gallery' => array(self::BELONGS_TO, 'Photogallery', array('id_photogallery_instance' => 'id_photogallery'), ),
      ));
    }

    public function last($limit=null) {
      $alias = $this->getTableAlias();
      $this->getDbCriteria()->mergeWith(array(
          'with' => 'gallery',
          'order' => "$alias.file DESC",
          'limit' => $limit,
      ));
      return $this;
    }

	}
?>