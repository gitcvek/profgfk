<?php

/**
 * Модель для таблицы "pr_event".
 *
 * The followings are the available columns in table 'pr_event':
 * @property integer $id_event
 * @property string $title
 * @property integer $is_visible
 * @property string $date
 * @property integer $photo
 * @property string $short
 * @property string $content
 *
 * The followings are the available model relations:
 * @property File $photoFile
 */
class Event extends DaActiveRecord {

  const ID_OBJECT = 'project-meropriyatiya';

  protected $idObject = self::ID_OBJECT;

  /**
   * Returns the static model of the specified AR class.
   * @param string $className active record class name.
   * @return Event the static model class
   */
  public static function model($className = __CLASS__) {
    return parent::model($className);
  }

  /**
   * @return string the associated database table name
   */
  public function tableName() {
    return 'pr_event';
  }

  /**
   * @return array validation rules for model attributes.
   */
  public function rules() {
    return array(
      array('title, date, short, content', 'required'),
      array('is_visible, photo', 'numerical', 'integerOnly'=>true),
      array('title', 'length', 'max'=>255),
      array('date', 'length', 'max'=>10),
    );
  }

  /**
   * @return array relational rules.
   */
  public function relations() {
    return array(
      'photoFile' => array(self::BELONGS_TO, 'File', 'photo'),
    );
  }

  public function behaviors() {
    $behaviors = array(
      'ImagePreviewBehavior' => array(
       'class' => 'ImagePreviewBehavior',
       'imageProperty' => 'photoFile',
       'formats' => array(
         '_list' => array(
           'width' => 531,
           'height' => 350,
         ),
       ),
      ),
    );
    return $behaviors;
  }

  /**
   * @return array customized attribute labels (name=>label)
   */
  public function attributeLabels() {
    return array(
      'id_event' => 'ID',
      'title' => 'Заголовок',
      'is_visible' => 'Видимость',
      'date' => 'Дата',
      'photo' => 'Картинка',
      'short' => 'Краткое содержание',
      'content' => 'Содержание',
    );
  }

  public function getUrl() {
    if ($this->id_event) {
      return Yii::app()->createUrl('event/default/view', array('id' => $this->id_event));
    }
    return Yii::app()->createUrl('event/default/index');
  }

  public function last($limit=null) {
    $alias = $this->getTableAlias();
    $this->getDbCriteria()->mergeWith(array(
        'with' => 'photoFile',
        'order' => "$alias.date DESC",
        'limit' => $limit,
    ));
    return $this;
  }

}