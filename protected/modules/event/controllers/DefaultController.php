<?php

class DefaultController extends Controller {

  protected $urlAlias = "event";  
  
  /**
   * Список новостей
   */
  public function actionIndex() {
    $criteria = new CDbCriteria();
    $criteria->scopes = array('last');
    
    $newsModule = $this->getModule();
    $category = null;
    $categories = array();
    //Если включено отображение категорий
    $count = Event::model()->count($criteria);
    
    $pages = new CPagination($count);
    $pages->pageSize = $newsModule->itemsCountPerPage;
    $pages->applyLimit($criteria);
    
    $event = Event::model()->findAll($criteria);

    $this->render('/index', array(
      'event' => $event,  // список новостей
      'pages' => $pages,  // пагинатор
    ));
  }
  
  /**
   * Одиночная новость
   * @param int $id
   */
  public function actionView($id) {
    $event = $this->loadModelOr404('Event', $id);
    $this->caption = $event->title;
    $this->render('/view',array(
      'model'=>$event
    ));
  }
}