<?php

class EventModule extends DaWebModuleAbstract {

	public $itemsCountPerPage = 20;

	protected $_urlRules = array(
	  'event/<id:\d+>' => 'event/default/view',
	  'event' => 'event/default/index',
	);
	
	public function init() {
		$this->setImport(array(
			$this->id.'.models.*',
			$this->id.'.components.*',
		));
	}

}
