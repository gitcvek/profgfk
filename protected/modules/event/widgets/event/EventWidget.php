<?php
class EventWidget extends DaWidget implements IParametersConfig {
  
  /**
   * Количество отображаемых новостей
   * @var int
   */
  public $maxNews = null;

  public static function getParametersConfig() {
    return array(
      'maxNews' => array(
        'type' => DataType::INT,
        'default' => 5,
        'label' => 'Количество отображаемых мероприятий',
        'required' => true,
      ),
    );
  }

  public function getEvent() {
    return Event::model()->last($this->maxNews)->findAll();
  }
  
  public function init() {
    if ($this->maxNews === null) {
      $this->maxNews = 5;
    }
    parent::init();
  }
  
  public function run() {
    $this->render('eventWidget');
  }
}