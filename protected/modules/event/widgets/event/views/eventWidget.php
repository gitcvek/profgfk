<?php
$this->registerCssFile('eventWidget.css');
?>
<div class="b-event-widget">
  <h2>Мероприятия</h2>
  <?php
  //если нужно брать переопределенное представление элемента списка из темы,
  //то вместо news.views._list_item нужно прописать
  //webroot.themes.название_темы.views.news._list_item
  ?>
<?php foreach ($this->getEvent() as $model): ?>
<?php $this->render('event.views._list_item', array('model' => $model)); ?>
<?php endforeach; ?>
<div class="archive"><a href="<?php echo Yii::app()->createUrl('/event/default/index/');?>">Все мероприятия</a></div>
</div>