<?php

//Подключаем css
$this->registerCssFile('event.css');

?>
<div class="b-event-list">
<?php foreach ($event as $model): ?>
<?php $this->renderPartial('/_list_item', array('model' => $model)); ?>
<?php endforeach; ?>
</div>

<?php  $this->widget('LinkPagerWidget', array(
  'pages' => $pages,
)); ?>


