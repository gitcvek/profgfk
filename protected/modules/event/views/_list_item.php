<div class="item">
  <?php if (($preview = $model->getImagePreview('_list')) !== null): ?>
    <div class="photo">
	<a href="<?php echo $model->getUrl(); ?>">
      <?php echo CHtml::link(CHtml::image($preview->getUrlPath(), $model->title), $model->getUrl(), array('title' => $model->title)); ?>
	   </a> 
    </div>
  <?php endif; ?>

  <h3><a href="<?php echo $model->getUrl(); ?>"><?php echo CHtml::link($model->title, $model->getUrl()); ?></a></h3>

  <div class="text"><a href="<?php echo $model->getUrl(); ?>"><?php echo nl2br($model->short); ?></a></div>

    <div class="date"><?php echo Yii::app()->dateFormatter->format(Yii::app()->getModule('news')->getListDateFormat(), $model->date); ?></div>
</div>